package com.zw;

import com.zw.bean.PageBean;
import com.zw.bean.User;
import com.zw.bean.User2;
import com.zw.util.ListUtils;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");
        List<User> users = new ArrayList<>();

        for (int i = 0; i < 15; i++) {
            User user = new User();
            user.setId(i);
            user.setName("姓名: " + i);
            user.setAge(i);
            users.add(user);
        }
        List<User2> user2s = ListUtils.copyList(users, User2.class);



        System.out.println(user2s.toString());

        PageBean pageBean = ListUtils.pageList(users, 1, 5);
        System.out.println(pageBean.toString());
    }


}