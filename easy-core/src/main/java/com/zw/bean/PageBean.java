package com.zw.bean;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 分页对象
 */
@Data
@ApiModel(value = "分页对象")
public class PageBean {

    @ApiModelProperty(value = "当前页")
    private int pageNum;

    @ApiModelProperty(value = "每页结果数")
    private int pageSize;

    @ApiModelProperty(value = "总页数")
    private int pages;

    @ApiModelProperty(value = "总数")
    private int total;

    @ApiModelProperty(value = "返回结果")
    private List list;
}
