package com.zw.bean;

import lombok.Data;

@Data
public class User {
    private Integer id;
    private String name;
    private int age;
}
