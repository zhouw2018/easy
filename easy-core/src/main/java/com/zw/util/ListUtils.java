package com.zw.util;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import com.zw.bean.PageBean;
import io.swagger.annotations.ApiModel;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

/**
 * List工具类
 *
 * @Author: ZHOUWU
 * @Date: 2021/12/18 10:01
 */
@Slf4j
@ApiModel(value = "List工具类")
public class ListUtils {

    /**
     * List复制
     *
     * @param source
     * @param targetClass
     * @param <T>
     * @return
     */
    public static <T> List<T> copyList(List source, Class<T> targetClass) {
        if (CollectionUtil.isNotEmpty(source) && targetClass != null) {
            List<T> targetList = new ArrayList<>(source.size());
            T target;
            for (int i = 0; i < source.size(); i++) {
                try {
                    target = targetClass.newInstance();
                    BeanUtil.copyProperties(source.get(i), target);
                } catch (Exception e) {
                    LOGGER.error("[List复制失败:{}]", e.getMessage());

                    throw new IllegalArgumentException(e.getMessage());
                }
                targetList.add(target);
            }
            return targetList;
        } else {
            return null;
        }
    }

    /**
     * list分页
     *
     * @param data     返回结果
     * @param pageNum  当前页,默认为1
     * @param pageSize 当前页,默认为10
     * @param <T>
     * @return
     */
    public static <T> PageBean pageList(List<T> data, int pageNum, int pageSize) {
        if (pageNum < 1 || pageSize < 1) {
            LOGGER.warn("pageList入参取默认值,pageNum:{},pageSize{}", pageNum, pageSize);
            pageNum = 1;
            pageSize = 10;
        }
        PageBean pageBean = new PageBean();
        if (data == null || data.isEmpty()) {
            pageBean.setPageNum(pageNum);
            pageBean.setPageSize(pageSize);
            pageBean.setPages(0);
            pageBean.setTotal(0);
            pageBean.setList(data);
        } else {
            List<T> list = new ArrayList<>();
            int currIdx = (pageNum > 1 ? (pageNum - 1) * pageSize : 0);
            for (int i = 0; i < pageSize && i < data.size() - currIdx; i++) {
                list.add(data.get(currIdx + i));
            }
            pageBean.setPageNum(pageNum);
            pageBean.setPageSize(pageSize);
            pageBean.setPages((data.size() - 1) / pageSize + 1);
            pageBean.setTotal(data.size());
            pageBean.setList(list);
        }

        return pageBean;
    }
}
